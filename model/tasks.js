module.exports = function () {
    var db = require('./../libs/connect_db')();
    var Schema = require("mongoose").Schema;

    var beacon = Schema({
        mac: String,
        cor: String,
        uuid: String,
        vinculo: String,

    })
    var animal = Schema({
        apelido: String,
        tipo: String,
        raca: String,
        cor: String,
        local: String,
        beacon: [beacon]

    })

    var cliente = Schema({
        nome_cliente: String,
        email: String,
        status: Boolean,
        animal: [animal]
    })

    return db.model('clientes', cliente);
}